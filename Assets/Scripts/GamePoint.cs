using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IGS520b.starter.SampleGame
{

    [RequireComponent(typeof(Collider))]
    public class GamePoint : MonoBehaviour
    {
        [Tooltip("Points scored by touching this object.")]
        public float points = 10;
        private bool isSelfDestructing = false;

        public System.Action<GamePoint> OnTriggerEnterAction;

        void Start()
        {
            // Make sure non of the colliders in child objects are active
            foreach (Collider collider in GetComponentsInChildren<Collider>())
            {
                collider.enabled = false;
            }

            // Make sure the root collider is a trigger and enabled
            Collider rootCollider = GetComponent<Collider>();
            rootCollider.enabled = true;
            rootCollider.isTrigger = true;
        }

        private void Update()
        {
            if (isSelfDestructing)
            {
                transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f) * Time.deltaTime;
            }

            if(this.transform.localScale.x < 0.1)
            {
                Destroy(gameObject);
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            OnTriggerEnterAction?.Invoke(this);
        }

        public void destroySelf()
        {
            isSelfDestructing = true;
        }
    }
}
