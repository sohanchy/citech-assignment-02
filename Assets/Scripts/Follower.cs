using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    private CharacterController controller;
    [SerializeField]
    float speed = 0.3f;
    
    private GameObject player;

    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("PlayerArmature");
        animator = gameObject.GetComponent<Animator>();
    }

    void FixedUpdate()
    {

        float dist = Vector3.Distance(player.transform.position, transform.position);
        if (dist > 5)
        {
            gameObject.transform.position += transform.forward * speed;
            animator.SetBool("Walk_Anim", true);
        }
        else
        {
            animator.SetBool("Walk_Anim", false);
        }

    }

    // Update is called once per frame
    void Update() 
    { 
        transform.LookAt(player.transform);
    }
}
