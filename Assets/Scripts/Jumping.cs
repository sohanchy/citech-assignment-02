using UnityEngine;
using System.Collections;

public class Jumping : MonoBehaviour
{
    public AnimationCurve myCurve;
    private GameObject player;

    private void Start()
    {
        player = GameObject.Find("PlayerArmature");
    }

    void Update()
    {
        float dist = Vector3.Distance(player.transform.position, transform.position);
        if (dist < 6)
        {
            transform.position = new Vector3(transform.position.x, myCurve.Evaluate((Time.time % myCurve.length)), transform.position.z);
        }

    }
}